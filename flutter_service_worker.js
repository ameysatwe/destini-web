'use strict';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/images/background.png": "cd760177ae72bdfca5cd8fa362b57d50",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "115e937bb829a890521f72d2e664b632",
"assets/FontManifest.json": "01700ba55b08a6141f33e168c4a6c22f",
"assets/fonts/MaterialIcons-Regular.ttf": "56d3ffdef7a25659eab6a68a3fbfaf16",
"assets/AssetManifest.json": "377e01a6d2492f8caf28064c3a8509ed",
"assets/LICENSE": "0000d255865246c9b55862d96ffd3089",
"manifest.json": "78f51c516efd3f27771a745c1598019e",
"index.html": "6289831561aa88f3128a16e68b49585e",
"/": "6289831561aa88f3128a16e68b49585e",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"main.dart.js": "c296d256fadc99533c02f92d99867128",
"favicon.png": "5dcef449791fa27946b3d35ad8803796"
};

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheName) {
      return caches.delete(cacheName);
    }).then(function (_) {
      return caches.open(CACHE_NAME);
    }).then(function (cache) {
      return cache.addAll(Object.keys(RESOURCES));
    })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          return response;
        }
        return fetch(event.request);
      })
  );
});
